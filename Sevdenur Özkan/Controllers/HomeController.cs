﻿

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Sevdenur_Özkan.Controllers
{
    
    public class HomeController : Controller
    {
        SiteEntities8 db = new SiteEntities8();


        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult KategoriCizgiRoman()
        {
            var books = db.Kitap.Where(x => x.ID > 0 && x.ID < 10);
            return View(books);
        }

        public ActionResult KategoriRoman()
        {
            var books = db.Kitap.Where(x => x.ID > 12 && x.ID < 22);
            return View(books);
        }

        public ActionResult KategoriDiger()
        {
           
            return View();
        }

        public ActionResult KitapSayfasi(int kitapId)
        {

            var model = db.Kitap.Find(kitapId);
            return View(model);


        }

 
        public ActionResult Dil(String LanguageAbbrevation)
        {
            if(LanguageAbbrevation != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(LanguageAbbrevation);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(LanguageAbbrevation);
            }

            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = LanguageAbbrevation;
            Response.Cookies.Add(cookie);

            return View("Index");


        }


     
    }
}