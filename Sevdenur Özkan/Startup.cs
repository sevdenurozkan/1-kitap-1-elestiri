﻿using Microsoft.Owin;
using Owin;


[assembly: OwinStartupAttribute(typeof(Sevdenur_Özkan.Startup))]
namespace Sevdenur_Özkan
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}