﻿using System.Web;
using System.Web.Optimization;

namespace Sevdenur_Özkan
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/Scripts/jquery-{version}.js"));


            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                     "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery1").Include(
                        "~/Scripts/jquery-3.1.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/b1").Include(
                    
                      "~/Scripts/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/b2").Include(
                  "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/npm").Include(
                 "~/Scripts/npm.js"));


            bundles.Add(new StyleBundle("~/Content/css1").Include(
                    "~/Content/bootstrap-theme.css"));

            bundles.Add(new StyleBundle("~/Content/css2").Include(
               "~/Content/bootstrap-theme.min.css"));

            bundles.Add(new StyleBundle("~/Content/css3").Include(
                  "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/css4").Include(
                "~/Content/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/css5").Include(
                 "~/Content/stylepadding.css"));

            bundles.Add(new StyleBundle("~/Content/css6").Include(
                 "~/Content/üyeol.css"));

            bundles.Add(new StyleBundle("~/Content/css7").Include(
                 "~/Content/YorumGözükmesi.css"));

            bundles.Add(new StyleBundle("~/Content/css8").Include(
                 "~/Content/yorumyapma.css"));







        }
    }
}
