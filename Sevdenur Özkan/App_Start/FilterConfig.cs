﻿using Sevdenur_Özkan.Controllers;
using System.Web;
using System.Web.Mvc;

namespace Sevdenur_Özkan
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new languageAttribute());
        }
    }
}
